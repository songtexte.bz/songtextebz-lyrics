# songtextebz-lyrics

Besides music, lyrics are an important part of a song. They are the words used by a composer to create a melody. Often, members of a band write the lyrics.